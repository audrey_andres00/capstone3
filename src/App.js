import Container from 'react-bootstrap/Container'
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import { useState, useEffect } from 'react';

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Profile from './pages/Profile';
import Register from './pages/Register';
import Products from './pages/Products';
import AddProduct from './pages/AddProduct';
import ProductView from './pages/ProductView';

import './App.css';
import { UserProvider } from './UserContext';


function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  // Function for clearing localStorage on logout
  const unsetUser = () => {

    localStorage.clear();
  };

  useEffect(() => {
    //console.log(user);
    fetch('https://cpstn2-ecommerceapi-andres.onrender.com/users/profile', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => {
        if (!res.ok) {
          // Check for 404 (Not Found) response
          if (res.status === 404) {
            // User profile not found, set user state accordingly
            setUser({
              id: null,
              isAdmin: null,
            });
            return null; // Return null to skip parsing the response
          }
          throw new Error('Network response was not ok');
        }
        return res.json(); // Parse the response if it's not an error
      })
      .then((data) => {
        if (data !== null) {
          //console.log(data);
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
          });
        }
      })
      .catch((error) => {
        console.error('Error fetching user profile:', error);
      });
  }, [localStorage.getItem('token')]);

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container fluid className="p-0">
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/register" element={<Register />} />
            <Route path="/products" element={<Products />} />
            <Route path="/admin/addproduct" element={<AddProduct />} />
            <Route path="/products/:productId" element={<ProductView />}/>
            
            {/*
            
            <Route path="/searchByPrice" element={<SearchByPrice />} />
            <Route path="*" element={<Error />} />*/}
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
