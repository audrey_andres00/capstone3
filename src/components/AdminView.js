import { useState, useEffect, useMemo } from 'react';
import { Table } from 'react-bootstrap';

import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct';

export default function AdminView({ productsData, fetchData }) {
  const [products, setProducts] = useState([]);

  const memoizedProducts = useMemo(() => {

    const data = Array.isArray(productsData) ? productsData : [];

    return data.map(product => (
      <tr key={product._id}>
        <td>{product._id}</td>
        <td>{product.productName}</td>
        <td>{product.description}</td>
        <td>{product.price}</td>
        <td>{product.stocks}</td>
        <td className={product.isActive ? 'text-success' : 'text-danger'}>
          {product.isActive ? 'Available' : 'Unavailable'}
        </td>
        <td>
          <EditProduct product={product._id} fetchData={fetchData} />
        </td>
        <td>
          <ArchiveProduct
            product={product._id}
            isActive={product.isActive}
            fetchData={fetchData}
          />
        </td>
      </tr>
    ));
  }, [fetchData]);

  useEffect(() => {
    setProducts(memoizedProducts);
  }, [memoizedProducts]);

  return (
    <>
      <h1 className="text-center my-4">Admin Dashboard</h1>

      <Table striped bordered hover responsive>
        <thead>
          <tr className="text-center">
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Stock</th>
            <th>Availability</th>
            <th colSpan="2">Status</th>
          </tr>
        </thead>

        <tbody className="text-center">{products}</tbody>
      </Table>
    </>
  );
}
