import { useState, useContext } from 'react';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Form, Button } from 'react-bootstrap';

import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
/*import '../App.css';*/

export default function AppNavbar(){
	const { user } = useContext(UserContext);

	return(
		<Navbar className="custom-navbar" sticky="top" expand="lg">
			<Container fluid>
			    <Navbar.Brand as={Link} to="/">
			    	<img src="/chuki-logo.png" alt="ChukiChuki logo" width="50" height="50"/>
			    </Navbar.Brand>

			    <Navbar.Toggle aria-controls="basic-navbar-nav" />
			    <Navbar.Collapse id="basic-navbar-nav">
				    <Nav className="ms-auto">
				        <Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
				        <Nav.Link as={NavLink} to="/products" exact>Products</Nav.Link>
				        
						{(user.id !== null) ?

							user.isAdmin ?
							<>
								<Nav.Link as={Link} to="/admin/addproduct">Add Products</Nav.Link>
								<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
							</>
							:
							<>
								<Nav.Link as={Link} to="/profile">Profile</Nav.Link>
								<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
							</>
						:
						<>
							<Nav.Link as={Link} to="/login">Login</Nav.Link>
							<Nav.Link as={Link} to="/register">Register</Nav.Link>
						</>
						}
						   
				    </Nav>
				    <Form className="d-flex d-md-none">
				            <Form.Control
				              type="search"
				              placeholder="Search"
				              className="me-2"
				              style={{ width: '250px', borderRadius: '25px' }}
				              aria-label="Search"
				            />
				    </Form>
			    </Navbar.Collapse>
			</Container>
		</Navbar>
		)
}