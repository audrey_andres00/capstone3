import { Carousel, Container } from 'react-bootstrap';

export default function Banner({data}) {

	return(
		<Container fluid className="p-0" >
			<Carousel>
		      <Carousel.Item interval={1000}>
		      	<img src="/nct-dream-sweatshirt-front-back-2.png" alt= "Kpop Collection" style={{ width: '100%', height: '90vh', objectFit: 'cover' }}/>
		        <Carousel.Caption>
		          <h3>KPop Shirts</h3>
		          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
		        </Carousel.Caption>
		      </Carousel.Item>
		      <Carousel.Item interval={500}>
		        <img src="/statements-front-back-1.png" alt="Statement Shirts" style={{ width: '100%', height: '90vh', objectFit: 'cover' }}/>
		        <Carousel.Caption>
		          <h3>Statement Shirts up to 30% OFF!</h3>
		          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
		        </Carousel.Caption>
		      </Carousel.Item>
		      <Carousel.Item>
		        <img src="/pkmn-jacket-front-back-2.png" alt="Pokemon X ChukiChuki Hoodie Collection" style={{ width: '100%', height: '90vh', objectFit: 'cover' }}/>
		        <Carousel.Caption>
		          <h3>Gotta Catch 'em ALL! - Pokemon X ChukiChuki Hoodie Collection</h3>
		          <p>
		            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
		          </p>
		        </Carousel.Caption>
		      </Carousel.Item>
		    </Carousel>
		</Container>
	)
}