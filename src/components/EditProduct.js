import React, { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function EditProduct({product, fetchData}){

	const [productId, setProductId] = useState('');

	const [productName, setProductName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [stocks, setStocks] = useState('');
	const [image, setImage] = useState('');

	const [showEdit, setShowEdit] = useState(false);

	const openEdit = (productId) => {

		setProductId(productId);
		setShowEdit(true)

		fetch(`https://cpstn2-ecommerceapi-andres.onrender.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setProductId(data._id);
			setProductName(data.productName);
			setDescription(data.description);
			setPrice(data.price);
			setStocks(data.stocks);
			setImage(data.image)
		})
	}

	const closeEdit = () => {
		setShowEdit(false)
		setProductName('')
		setDescription('')
		setPrice(0)
		setStocks(0)
		setImage('')
	}

	const handleImageChange = (e) => {
	    const selectedImage = e.target.files[0];
	    setImage(selectedImage);
	};

	//function to update the course
	const editProduct = (e) => {
		e.preventDefault();

		const formData = new FormData();
	    formData.append('productName', productName);
	    formData.append('description', description);
	    formData.append('price', price);
	    formData.append('stocks', stocks);
	    formData.append('image', image);

		fetch(`https://cpstn2-ecommerceapi-andres.onrender.com/products/update/${productId}`,{
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: formData,
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data.message === 'Product successfully updated!'){
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Product Successfully Updated'
				})
				closeEdit();
				fetchData();
			}else{
				Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: 'Please try again'
				})
				closeEdit();
				fetchData();
			}
		})
	}

	return(
		<>
			<Button variant="primary" size="sm" onClick={() => openEdit(product)}> Edit </Button>

		{/*Edit Modal Forms*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Course</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control 
								type="text" 
								value={productName} 
								onChange={e => setProductName(e.target.value)} />
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control 
								type="text" 
								value={description} 
								onChange={e => setDescription(e.target.value)} />
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control 
								type="number" 
								value={price} 
								onChange={e => setPrice(e.target.value)} />
						</Form.Group>

						<Form.Group>
							<Form.Label>Stock</Form.Label>
							<Form.Control 
								type="number" 
								value={stocks} 
								onChange={e => setStocks(e.target.value)} />
						</Form.Group>

						<Form.Group>
					        <Form.Label>Image URL</Form.Label>
							<Form.Control 
								type="text" 
								value={image} 
								onChange={e => setImage(e.target.value)} />
					    </Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
				
			</Modal>
		</>
	)
}