import { useState, useEffect } from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
//import PropTypes from 'prop-types';

export default function ProductCard() {

	const [products, setProducts] = useState([]);

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        // Fetch a list of products
        const productsResponse = await fetch('https://cpstn2-ecommerceapi-andres.onrender.com/products');
        const productData = await productsResponse.json();
        //console.log(productData);

        // Set the state with products
        setProducts(productData);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchProducts();
  }, []);

  return (
    <Container fluid className="p-3 mt-2 d-flex justify-content-center">
      <Row id="product-grid" className="d-flex justify-content-center">
        {products.map((product, index) => (
              <Col key={index} md={4}>
                  <Card className="mb-4 rounded-0">
                    <Card.Img
                      variant="top"
                      src={product.image}
                      alt={product.productName}
                      style={{ objectFit: 'cover', height: '100%', width: '100%'}}
                    />
                    <Card.Body>
                      <Card.Title>{product.productName}</Card.Title>
                      <Card.Subtitle>Price: {product.price}</Card.Subtitle>
                      <Link className="btn btn-primary mt-2" to={`/products/${product._id}`}>
                        Add to Cart
                      </Link>
                    </Card.Body>
                  </Card>
              </Col>
            ))}
      </Row>
    </Container>
  );
}