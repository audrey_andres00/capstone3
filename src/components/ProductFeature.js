import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Container, Card, Button, Col, Row } from 'react-bootstrap';

export default function ProductFeature() {
	const [products, setProducts] = useState([]);

	useEffect(() => {
	  const fetchProducts = async () => {
	    try {
	      // Fetch a list of products
	      const productsResponse = await fetch('https://cpstn2-ecommerceapi-andres.onrender.com/products');
	      const productData = await productsResponse.json();
	      //console.log(productData);

	      // Set the state with products
	      setProducts(productData);
	    } catch (error) {
	      console.error('Error fetching data:', error);
	    }
	  };

	  fetchProducts();
	}, []);

	return(
		<Container fluid className="p-3 mt-2 d-flex justify-content-center">
			<Row id="feature" className="d-flex justify-content-center">
				{products.map((product, index) => (
		          <Col key={index} md={4}>
		          	<Link to={`/products/${product._id}`}>
			            <Card className="text-white mb-4 rounded-0" id="productCard">
			              <Card.Img
			                src={product.image}
			                alt={product.productName}
			                style={{ objectFit: 'cover', height: '100%', width: '100%'}}
			              />
			            </Card>
		            </Link>
		          </Col>
		        ))}
			</Row>
		</Container>
	)
}