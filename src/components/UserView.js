import React, { useState, useEffect } from 'react';
import ProductCard from './ProductCard';
import ProductSearch from './ProductSearch';

export default function UserView() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    const fetchDataForUserView = async () => {
      try {
        const response = await fetch('https://cpstn2-ecommerceapi-andres.onrender.com/products', {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
          },
        });

        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }

        const data = await response.json();
        // Filter products where isActive is true
        const activeProducts = data.filter((product) => product.isActive === true);
        // Map the filtered products to ProductCard components
        const productCards = activeProducts.map((product) => (
          <ProductCard productProp={product} key={product._id} />
        ));

        setProducts(productCards);
      } catch (error) {
        console.error('Error fetching product data:', error);
      }
    };

    fetchDataForUserView();
  }, []);

  return(
    <>
      <ProductSearch />
      <div className="mt-5">{products}</div>
    </>
  );
}
