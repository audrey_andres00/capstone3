import {useState,useEffect, useContext} from 'react';
import {Form,Button} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function AddProduct() {
	const navigate = useNavigate();

	const {user} = useContext(UserContext);

	const [productName, setProductName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState(0);
	const [image, setImage] = useState('');

	async function addProduct(e){

		e.preventDefault();

		const token = localStorage.getItem('token');

		//console.log(token);

	    try{
	    	const response = await fetch('https://cpstn2-ecommerceapi-andres.onrender.com/products/admin/addproduct',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				productName: productName,
				description: description,
				price: price,
				stocks: stocks,
				image: image
			})
		})

	    const data = await response.json();

	      if (!response.ok) {
	        Swal.fire({
	        icon: 'error',
	        title: 'An error occurred',
	        text: data.message
	      });
	      }else{
	      	Swal.fire({
	          icon: 'success',
	          title: data.message,
	        });
	      } 
	    

		setProductName('')
		setDescription('')
		setPrice(0)
		setStocks(0)
		setImage('')
	} catch (error) {
      console.error('Error:', error);
    }
		
}

	return(
		(user.isAdmin === true)
            ?
            <>
                <h1 className="my-5 text-center">Add Product</h1>
                <Form onSubmit={e => addProduct(e)}>
                    <Form.Group>
                        <Form.Label>Name:</Form.Label>
                        <Form.Control
                        	type="text" 
                        	placeholder="Enter Name" 
                        	required 
                        	value={productName} 
                        	onChange={e => setProductName(e.target.value)}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Description:</Form.Label>
                        <Form.Control
                        	type="text"
                        	placeholder="Enter Description"
                        	required
                        	value={description}
                        	onChange={e => setDescription(e.target.value)}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Price:</Form.Label>
                        <Form.Control
                        	type="number"
                        	placeholder="Enter Price"
                        	required
                        	value={price}
                        	onChange={e => setPrice(e.target.value)}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Stocks:</Form.Label>
                        <Form.Control
                        	type="number"
                        	placeholder="Enter Price"
                        	required
                        	value={stocks}
                        	onChange={e => setStocks(e.target.value)}/>
                    </Form.Group>
                    <Form.Group>
			            <Form.Label>Image:</Form.Label>
			            <Form.Control
			            	type="text"
			            	placeholder="Enter Image URL"
			            	required
			            	value={image}
			            	onChange={e => setImage(e.target.value)}/>
			        </Form.Group>
                    <Button variant="primary" type="submit" className="my-5">Submit</Button>
                </Form>
		    </>
            :
            <Navigate to="/products" />
	)
}