import { Container } from 'react-bootstrap';

import Banner from '../components/Banner';
import ProductFeature from '../components/ProductFeature';

export default function Home() {

	return(
		<Container fluid className="p-0">
			<>
				<Banner />
				<ProductFeature />
			</>
		</Container>
	)
}