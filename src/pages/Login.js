import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom'; 
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {
	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(true);

	function authenticate(e) {
	  e.preventDefault();
	  fetch('https://cpstn2-ecommerceapi-andres.onrender.com/users/login', {
	    method: 'POST',
	    headers: {
	      "Content-Type": "application/json"
	    },
	    body: JSON.stringify({
	      email: email,
	      password: password
	    })
	  })
	  .then(res => res.json())
	  .then(data => {
	    if (data.access) {
	      localStorage.setItem('token', data.access);
	      retrieveUserDetails(data.access);
	      setUser({
	        access: localStorage.getItem('token')
	      });
	      const Toast = Swal.mixin({
	        toast: true,
	        position: 'top-right',
	        iconColor: 'white',
	        customClass: {
	          popup: 'colored-toast'
	        },
	        showConfirmButton: false,
	        timer: 1500,
	        timerProgressBar: true
	      });
	      Toast.fire({
	        icon: 'success',
	        title: 'Success'
	      });
	    } else {
	      const Toast = Swal.mixin({
	        toast: true,
	        position: 'top-right',
	        iconColor: 'white',
	        customClass: {
	          popup: 'colored-toast'
	        },
	        showConfirmButton: false,
	        timer: 1500,
	        timerProgressBar: true
	      });
	      Toast.fire({
	        icon: 'error',
	        title: 'Error'
	      });
	    }
	    setEmail('');
	    setPassword('');
	  });
	}

    const retrieveUserDetails = (token) => {
        
        fetch('https://cpstn2-ecommerceapi-andres.onrender.com/users/profile', {
            headers: {
                Authorization: `Bearer ${ token }`
            }
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            setUser({
              id: data._id,
              isAdmin: data.isAdmin
            });

        })

    };

	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	}, [email, password]);

	return(

		(user.id !== null) ?

			<Navigate to="/products" />
			:
			<Form onSubmit={(e) => authenticate(e)} className="login-form" >
				<h1 className="my-5 text-center">Login</h1>
	            <Form.Group className="my-3" controlId="userEmail">
	                <Form.Label>Email address</Form.Label>
	                <Form.Control 
	                    type="email" 
	                    placeholder="Enter email"
	                    value={email}
            			onChange={(e) => setEmail(e.target.value)}
	                    required
	                />
	            </Form.Group>

	            <Form.Group controlId="password">
	                <Form.Label>Password</Form.Label>
	                <Form.Control 
	                    type="password" 
	                    placeholder="Password"
	                    value={password}
            			onChange={(e) => setPassword(e.target.value)}
	                    required
	                />
	            </Form.Group>

	            <div className="d-flex justify-content-center">
		             { isActive ? 
		                <Button className="mt-3" type="submit" id="submitActiveBtn">
		                    Login
		                </Button>
		                : 
		                <Button className="mt-3" variant="disabled" type="submit" id="submitBtn" disabled>
		                    Login
		                </Button>
		            }
	            </div>
			</Form>
	)
}