import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout() {
  const { unsetUser, setUser } = useContext(UserContext);

  unsetUser();

  useEffect(() => {
    console.log('Logging out...');
    setUser({
    	id: null,
    	isAdmin: null
    })
  }, []);

  // After unsetUser is called, user state will be null,
  // and this effect will trigger a navigation to the login page
  return(
  	//null
  	<Navigate to='/' />
  );
}