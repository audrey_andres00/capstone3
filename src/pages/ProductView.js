import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {
	const {productId} = useParams();
	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const [productName, setProductName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(1);
	const [image, setImage] = useState('');

	const checkout = (productId) => {
		fetch('https://cpstn2-ecommerceapi-andres.onrender.com/orders/checkout',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				productName: productName,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data.message);

			if(data.message === "Order placed successfully"){
				Swal.fire({
					title: "Order placed!",
					icon: 'success'
				});

				navigate("/products");
			}else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}
		})
	}

	useEffect(() => {
		console.log(productId);

		fetch(`https://cpstn2-ecommerceapi-andres.onrender.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setImage(data.image);
			setProductName(data.productName);
			setDescription(data.description);
			setPrice(data.price);

			if(data.quantity !== undefined){
				setQuantity(data.quantity);
			}
		})
	}, [productId])

	return(
		<Container className="mt-5">
			<Row>
				<Col>
					<img
						src={image}
	                    alt={productName}
	                    style={{ objectFit: 'cover', height: '100%', width: '100%'}}/>
				</Col>
				<Col lg={{ span: 6, offset: 3 }} className="mb-5">
					<Card>
						<Card.Body className="align-items-center">
							<Card.Title>{productName}</Card.Title>
							<Card.Subtitle className="mt-3">Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
							
							<Row className="mb-4">
				              <Col className="d-flex align-items-center">
								  <Card.Subtitle className="me-2">Quantity:</Card.Subtitle>
								  <Form.Group className="mb-0">
								    <Form.Control
								      type="number"
								      value={quantity}
								      onChange={(e) => setQuantity(e.target.value)}
								      min="1"
								      style={{ width: '80px' }}
								    />
								  </Form.Group>
								</Col>
				            </Row>

							{ user.id !== null ? 
									<Button variant="primary" block onClick={() => checkout(productId)}>Checkout</Button>
								: 
									<Link className="btn btn-danger btn-block" to="/login">Log in to Order</Link>
							}
						</Card.Body>
					</Card>
				</Col>
				
			</Row>
		</Container>
	)
}